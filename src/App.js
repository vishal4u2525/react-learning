import React from 'react';
import './App.css';
import Person from './Person/Person';

class App extends React.Component {
	state = {
		persons: [
			{ name: 'Vishal', age: 28 },
			{ name: 'Sejal', age: 25 },
			{ name: 'Jihan', age: 2 },
		],
	};

	switchNameHandler = (newName) => {
		this.setState({
			persons: [
				{ name: newName, age: 55 },
				{ name: 'Sejal', age: 25 },
				{ name: 'Jihan', age: 25 },
			],
		});
	};

	nameChangeHandler = (event) => {
		this.setState({
			persons: [
				{ name: 'Vishal', age: 55 },
				{ name: event.target.value, age: 25 },
				{ name: 'Jihan', age: 25 },
			],
		});
	};
	render() {
		return (
			<div className='App'>
				<h1>Hello World !</h1>
				<p>It's working</p>
				<button onClick={() => this.switchNameHandler('GSP')}>Switch Name</button>
				<Person name={this.state.persons[0].name} age={this.state.persons[0].age} />
				<Person
					name={this.state.persons[1].name}
					age={this.state.persons[1].age}
					click={this.switchNameHandler.bind(this, 'vishal')}
					change={this.nameChangeHandler}
				>
					Hobbies: Games
				</Person>
				<Person name={this.state.persons[2].name} age={this.state.persons[2].age} />
			</div>
		);
		// return React.createElement(
		// 	'div',
		// 	{ className: 'App' },
		// 	React.createElement('h1', { className: 'title' }, 'Vishal Here')
		// );
	}
}

export default App;
