import React from 'react';

const person = (props) => {
	const { name, click, children, age, change } = props;
	return (
		<div>
			<p onClick={click}>
				I am {name}. I am {age} year old.
			</p>
			<p>{children}</p>
			<input type='text' onChange={change} value={name} />
		</div>
	);
};

export default person;
