import React, { useState } from 'react';
import './App.css';
import Person from './Person/Person';

const App = (props) => {
	const [personState, setPersonState] = useState({
		persons: [
			{ name: 'Vishal', age: 28 },
			{ name: 'Sejal', age: 25 },
			{ name: 'Jihan', age: 2 },
		],
	});
	const [otherState, setOtherState] = useState('This is simple string');
	const switchNameHandler = () => {
		setPersonState({
			persons: [
				{ name: 'Ghanshyambhai', age: 55 },
				{ name: 'Sejal', age: 25 },
				{ name: 'Jihan', age: 25 },
			],
		});
		setOtherState('updated string');
	};
	return (
		<div className='App'>
			<h1>Hello World !</h1>
			<p>It's working</p>
			<button onClick={switchNameHandler}>Switch Name</button>
			<Person name={personState.persons[0].name} age={personState.persons[0].age} />
			<Person name={personState.persons[1].name} age={personState.persons[1].age}>
				Hobbies: Games
			</Person>
			<Person name={personState.persons[2].name} age={personState.persons[2].age} />
			<p>{otherState}</p>
		</div>
	);
	// return React.createElement(
	// 	'div',
	// 	{ className: 'App' },
	// 	React.createElement('h1', { className: 'title' }, 'Vishal Here')
	// );
};

export default App;
